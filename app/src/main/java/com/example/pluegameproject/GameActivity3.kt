package com.example.pluegameproject

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.TextView
import kotlin.random.Random

private var countWin = 0
private var countLose = 0
private var sum = 0
private val intent = Intent()
private var num1 = Random.nextInt(0, 11)
private var num2 = Random.nextInt(0, 11)

class GameActivity3 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game3)

        StartGame()
//        Handler().postDelayed({
//            sum = 0
//        }, 2000)
    }

    private fun StartGame() {
        sum = 0
        sum = Game()
        intent.putExtra("Win", countWin)
        intent.putExtra("Lose", countLose)
        setResult(Activity.RESULT_OK, intent)
        Check(sum)
        ResultCount()
    }

    private fun Check(sum: Int) {
        val btnSum7 = findViewById<Button>(R.id.btnSum7)
        val btnSum8 = findViewById<Button>(R.id.btnSum8)
        val btnSum9 = findViewById<Button>(R.id.btnSum9)

        val ranBtn = Random.nextInt(1, 3)

        if (ranBtn == 1) {
            btnSum7.text = sum.toString()
            btnSum8.text = (sum + 1).toString()
            btnSum9.text = (sum + 2).toString()
        }
        if (ranBtn == 2) {
            btnSum7.text = (sum - 1).toString()
            btnSum8.text = sum.toString()
            btnSum9.text = (sum + 1).toString()
        }
        if (ranBtn == 3) {
            btnSum7.text = (sum - 2).toString()
            btnSum8.text = (sum - 1).toString()
            btnSum9.text = sum.toString()
        } else {
            btnSum7.setOnClickListener {
                if (btnSum7.text == sum.toString()) {
                    countWin += 1
                    StartGame()
                } else {
                    countLose += 1
                    StartGame()
                }
            }
            btnSum8.setOnClickListener {
                if (btnSum8.text == sum.toString()) {
                    countWin += 1
                    StartGame()
                } else {
                    countLose += 1
                    StartGame()
                }
            }
            btnSum9.setOnClickListener {
                if (btnSum9.text == sum.toString()) {
                    countWin += 1
                    StartGame()
                } else {
                    countLose += 1
                    StartGame()
                }
            }
        }
    }

    private fun ResultCount(): Pair<TextView, TextView> {
        val textCountWin3 = findViewById<TextView>(R.id.textCountWin3)
        val textCountLose3 = findViewById<TextView>(R.id.textCountLose3)
        textCountWin3.text = "Win: "+countWin.toString()
        textCountLose3.text = "Lose: "+countLose.toString()
        return Pair(textCountWin3, textCountLose3)
    }

    private fun Game(): Int {
        val textNum5 = findViewById<TextView>(R.id.textNum5)
        val textNum6 = findViewById<TextView>(R.id.textNum6)
        num1 = Random.nextInt(0, 11)
        num2 = Random.nextInt(0, 11)
        textNum5.text = num1.toString()
        textNum6.text = num2.toString()

        return num1 * num2
    }
}