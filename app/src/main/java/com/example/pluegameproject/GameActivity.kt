package com.example.pluegameproject

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.TextView
import kotlin.random.Random

private var countWin = 0
private var countLose = 0
private var sum = 0
private var intent = Intent()
private var num1 = Random.nextInt(0, 11)
private var num2 = Random.nextInt(0, 11)

class GameActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        StartGame()
    }
    private fun StartGame(){
        sum = 0
        sum = Game()
        intent.putExtra("Win", countWin)
        intent.putExtra("Lose", countLose)
        setResult(Activity.RESULT_OK, intent)
        Check(sum)
        ResultCount()

    }

    private fun Check(sum: Int) {
        val btnSum1 = findViewById<Button>(R.id.btnSum1)
        val btnSum2 = findViewById<Button>(R.id.btnSum2)
        val btnSum3 = findViewById<Button>(R.id.btnSum3)

        val ranBtn = Random.nextInt(1, 3)

        if (ranBtn == 1) {
            btnSum1.text = sum.toString()
            btnSum2.text = (sum + 1).toString()
            btnSum3.text = (sum + 2).toString()
        }
        if (ranBtn == 2) {
            btnSum1.text = (sum - 1).toString()
            btnSum2.text = sum.toString()
            btnSum3.text = (sum + 1).toString()
        }
        if (ranBtn == 3) {
            btnSum1.text = (sum - 2).toString()
            btnSum2.text = (sum - 1).toString()
            btnSum3.text = sum.toString()
        } else {
            btnSum1.setOnClickListener {
                if (btnSum1.text == sum.toString()) {
                    countWin += 1
                    StartGame()
                } else {
                    countLose += 1
                    StartGame()
                }
            }
            btnSum2.setOnClickListener {
                if (btnSum2.text == sum.toString()) {
                    countWin += 1
                    StartGame()
                } else {
                    countLose += 1
                    StartGame()
                }
            }
            btnSum3.setOnClickListener {
                if (btnSum3.text == sum.toString()) {
                    countWin += 1
                    StartGame()
                } else {
                    countLose += 1
                    StartGame()
                }
            }
        }
    }

    private fun ResultCount(): Pair<TextView, TextView> {
        val textCountWin = findViewById<TextView>(R.id.textCountWin)
        val textCountLose = findViewById<TextView>(R.id.textCountLose)
        textCountWin.text = "Win: "+countWin.toString()
        textCountLose.text = "Lose: "+countLose.toString()
        return Pair(textCountWin, textCountLose)
    }

    private fun Game(): Int {
        val textNum1 = findViewById<TextView>(R.id.textNum1)
        val textNum2 = findViewById<TextView>(R.id.textNum2)
        num1 = Random.nextInt(0, 11)
        num2 = Random.nextInt(0, 11)
        textNum1.text = num1.toString()
        textNum2.text = num2.toString()

        return num1 + num2
    }
}