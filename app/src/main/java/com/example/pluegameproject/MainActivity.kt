package com.example.pluegameproject

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

private var QUESTION_ACTIVITY_CODE = 0
private var True = 0
private var False = 0
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnPlus = findViewById<Button>(R.id.btnPlus)
        btnPlus.setOnClickListener {
            val intent = Intent(MainActivity@this, GameActivity::class.java)
            startActivityForResult(intent, 0)
        }

        val btnMinus = findViewById<Button>(R.id.btnMinus)
        btnMinus.setOnClickListener {
            val intent = Intent(MainActivity@this, GameActivity2::class.java)
            startActivityForResult(intent, 0)
        }
//
        val btnMulti = findViewById<Button>(R.id.btnMulti)
        btnMulti.setOnClickListener {
            val intent = Intent(MainActivity@this, GameActivity3::class.java)
            startActivityForResult(intent, 0)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == QUESTION_ACTIVITY_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                val textViewWin = findViewById<TextView>(R.id.textViewWin)
                val textViewLose = findViewById<TextView>(R.id.textViewLose)

                var returnCount1 = data!!.getIntExtra("Win", True)
                True += returnCount1
                textViewWin.text = returnCount1.toString()
                val returnCount2 = data!!.getIntExtra("Lose", False)
                False += returnCount2
                textViewLose.text = returnCount2.toString()
                textViewWin.text = "$returnCount1"
                textViewLose.text = "$returnCount2"
            }
        }

    }
}